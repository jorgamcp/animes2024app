﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Anime2024App.Migrations
{
    /// <inheritdoc />
    public partial class StatusAnimeAdded : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "Animes",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "Animes");
        }
    }
}
