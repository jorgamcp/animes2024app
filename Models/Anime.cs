using System.ComponentModel.DataAnnotations;


namespace Models
{
    public class Anime
    {


        public int IdAnime { get; set; }

        [Required(ErrorMessage = "The Anime Name cannot be empty"), StringLength(500)] 
        public string Name { get; set; } = null!;

        [StringLength(500, ErrorMessage = "Max Characters 500.")]
        public string NameEnglish { get; set; } = null!;

        [Required(ErrorMessage = "The Release Year of Anime  is required")]
        public int ReleaseYear { get; set; }
        public string SeasonPremiere { get; set; } = null!;

        public string Status {get;set; } = "undefined";

    }
}