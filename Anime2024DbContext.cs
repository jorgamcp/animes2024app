using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Models;
public class Anime2024DbContext : DbContext
{
    public Anime2024DbContext(DbContextOptions<Anime2024DbContext> options)
        :base (options)
    {
        
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.Entity<Anime>().HasKey(a => a.IdAnime);
    }

    public DbSet<Anime> Animes {get;set;}
}