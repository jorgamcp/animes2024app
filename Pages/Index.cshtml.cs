using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace Anime2024App.Pages;

public class IndexModel : PageModel
{
    private readonly ILogger<IndexModel> _logger;

    private readonly Anime2024DbContext _anime2024DbContext;

    public IList<Models.Anime> Animes {get;set;} =default!;

    public IndexModel(Anime2024DbContext anime2024DbContext,ILogger<IndexModel> logger)
    {
        _anime2024DbContext=anime2024DbContext;
        _logger=logger;
    }

    public async Task<IActionResult> OnGetAsync()
    {
        if(_anime2024DbContext.Animes != null)
        {
            Animes = await _anime2024DbContext.Animes.ToListAsync();
        }

        return Page();
    }
}
