

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Models;

public class NewPageModel : PageModel
{
    // Contexto de Entity Framework Core. Clase que conecta con mi BBDD
    private readonly Anime2024DbContext _anime2024Dbcontext;

    // Enlace de Modelo BindProperty
    [BindProperty]
    public Anime? Anime { get; set; }

    private readonly ILogger<NewPageModel> _logger;

    public NewPageModel(ILogger<NewPageModel> logger, Anime2024DbContext anime2024DbContext)
    {
        _logger = logger;
        _anime2024Dbcontext = anime2024DbContext; // Inyeccion de dependencias.
    }

    public IActionResult OnGet()
    {
        return Page();
    }

    public async Task<IActionResult> OnPostAsync()
    {
        // check if model is valid. If not valid reshow form to user.
        if (!ModelState.IsValid)
        {
            return Page();
        }
        if (Anime != null)
        {
            _anime2024Dbcontext.Animes.Add(Anime);

        }
        // Esperamos a que se escriban los datos en la base de datos. Operación asincrónica toma tiempo
        // Dependiendo de la operación Ef Core traducirá SaveChangesAsync() a un INSERT,UPDATE o DELETE 
        await _anime2024Dbcontext.SaveChangesAsync();
        return RedirectToPage("../Index");
    }
}