


using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Models;

public class DeletePageModel : PageModel
{

    private readonly Anime2024DbContext _anime2024DbContext;

    [BindProperty]
    public Anime Anime { get; set; }

    private readonly ILogger<DeletePageModel> _logger;

    public DeletePageModel(
        ILogger<DeletePageModel> logger, Anime2024DbContext anime2024DbContext
    )
    {
        _logger = logger;
        _anime2024DbContext = anime2024DbContext;
    }

    public async Task<IActionResult> OnGetAsync(int IdAnime)
    {
        Anime = await _anime2024DbContext.Animes.FindAsync(IdAnime);

        if (Anime == null)
        {
            return NotFound();
        }
        return Page();
    }

    public async Task<IActionResult> OnPostAsync(int? IdAnime)
    {
        Anime? animeToDelete = await _anime2024DbContext.Animes.FindAsync(IdAnime);
        if (animeToDelete == null)
        {
            return NotFound();
        }
        else
        {
            _anime2024DbContext.Remove(animeToDelete);
            await _anime2024DbContext.SaveChangesAsync();
            return RedirectToPage("../Index");
        }
       
    }
}
