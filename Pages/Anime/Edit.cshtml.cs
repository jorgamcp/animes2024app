

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Models;

public class EditPageModel : PageModel
{
    // Contexto de Entity Framework Core. Clase que conecta con mi BBDD
    private readonly Anime2024DbContext _anime2024Dbcontext;

    // Enlace de Modelo BindProperty
    [BindProperty]
    public Anime Anime { get; set; }

    private readonly ILogger<NewPageModel> _logger;

    public EditPageModel(ILogger<NewPageModel> logger, Anime2024DbContext anime2024DbContext)
    {
        _logger = logger;
        _anime2024Dbcontext = anime2024DbContext; // Inyeccion de dependencias.
    }

    public async Task<IActionResult> OnGetAsync(int IdAnime)
    {
        
        Console.WriteLine(IdAnime);
         Anime = _anime2024Dbcontext.Animes.Where(a => a.IdAnime == IdAnime).First();
        Console.WriteLine("Status anime -> " + Anime.Status);
        if (Anime == null)
        {
            return NotFound();
        }
        return Page();
    }


    public async Task<IActionResult> OnPostAsync(int? IdAnime)
    {
        Anime? animeToUpdate = await _anime2024Dbcontext.Animes.FindAsync(IdAnime);
        
        if (animeToUpdate == null){
            return NotFound();
        }

        if(await TryUpdateModelAsync<Anime>(animeToUpdate,"anime",
            a => a.Name,
            a => a.NameEnglish,
            a => a.ReleaseYear,
            a => a.SeasonPremiere,
            a => a.Status
            )
        ){
            await _anime2024Dbcontext.SaveChangesAsync();
            return RedirectToPage("../Index");
            
        }
        return Page();
    }
}